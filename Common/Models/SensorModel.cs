﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Models
{
    public class SensorModel
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}