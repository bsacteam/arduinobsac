﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models.Api
{
    public class SensorInfoModel
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Id { get; set; }

        public List<SensorReadingModel> Readings { get; set; }
    }
}