﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Web.Results;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataContext _db;
        private readonly DbSet<Reading> _readingsRepository;
        public HomeController()
        {
            _db = new DataContext();
            _readingsRepository = _db.Set<Reading>();
        }
        //public ActionResult Index()
        //{
        //    ViewBag.Title = "Home Page";

        //    return View();
        //}

        public ActionResult Readings()
        {
            return View();
        }

        public ActionResult Chart(int sensorId)
        {
            return View(sensorId);
        }

        public ActionResult Analytics(int sensorId)
        {
            return View(sensorId);
        }

        public async Task<ActionResult> ExportValuesByPeriod(DateTime startPeriod, DateTime endPeriod, int sensorId, int? everyNth = null)
        {
            if (startPeriod > endPeriod)
            {
                throw new ArgumentException("Invalid dates");
            }
            var items = await _readingsRepository.Where(f => f.Created >= startPeriod && f.Created < endPeriod && f.SensorId == sensorId && !everyNth.HasValue || f.Id % everyNth.Value == 0).ToListAsync();
            return new ExcelFileResult<Reading>(items, "Readings");
        }
    }
}
