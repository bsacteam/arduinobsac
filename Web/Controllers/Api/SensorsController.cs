﻿using Common.Models;
using DAL;
using Entity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Web.Models;
using Web.Models.Api;
using Z.EntityFramework.Plus;

namespace ArduinoServer.Controllers.Api
{
    [RoutePrefix("api/sensors")]
    public class SensorsController : ApiController
    {
        private DataContext _db = new DataContext();

        
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var sensors = await _db.Sensors.Select(f => new SensorInfoModel
            {
                Id = f.Id,
                Latitude = f.Latitude,
                Longitude = f.Longitude,
                Readings = f.Readings.Where(z=>z.Created.Day == DateTime.UtcNow.Day).OrderByDescending(z=>z.Created).Select(z=> new SensorReadingModel {
                    CH4 = z.CH4,
                    CO = z.CO,
                    CO2 = z.CO2,
                    Dust = z.Dust,
                    Hum = z.Hum,
                    LPG = z.LPG,
                    Preassure = z.Preassure,
                    Temp = z.Temp,
                    Created = z.Created,
                }).Take(10).ToList()
            }).ToListAsync();
            return Ok(sensors);
        }


        [Route("checkTrackingKey")]
        [HttpGet]
        public async Task<IHttpActionResult> CheckTrackingKey([FromUri]string key)
        {
            return Ok(await _db.Set<Sensor>().AnyAsync(f => f.TrackingKey == key));
        }

        [HttpPost]
        public async Task<IHttpActionResult> Create(SensorModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var sensor = new Sensor
            {
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                TrackingKey = Guid.NewGuid().ToString()
            };
            _db.Set<Sensor>().Add(sensor);
            await _db.SaveChangesAsync();
            return Created("", sensor.TrackingKey);
        }

        [HttpPut]
        public async Task<IHttpActionResult> Update(string trackingKey, [FromBody]SensorModel model)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(trackingKey))
            {
                return BadRequest();
            }
            await _db.Set<Sensor>().UpdateAsync(f => new Sensor
            {
                Latitude = model.Latitude,
                Longitude = model.Longitude
            });
            await _db.SaveChangesAsync();
            return Ok(trackingKey);
        }
    }
}