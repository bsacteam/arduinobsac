﻿using Common.Models;
using DAL;
using Entity;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Web.Hub;
using Web.Models;
using Web.Models.Hub;

namespace ArduinoServer.Controllers.Api
{
    [RoutePrefix("api/readings")]
    public class ReadingsController : ApiController
    {
        private readonly DataContext _db;
        private readonly IHubContext<IReadingsClient> _hubContext;

        public ReadingsController()
        {
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<ReadingsHub, IReadingsClient>();
            _db = new DataContext();
        }


        // GET: api/Readings
        [HttpGet]
        public List<Reading> GetReadings()
        {
            var items = _db.Readings.OrderByDescending(f => f.Id).ToList();
            return items;
        }

        // GET: api/Readings/5
        [HttpGet]
        [ResponseType(typeof(Reading))]
        public IHttpActionResult GetReading(int id)
        {
            Reading reading = _db.Readings.Find(id);
            if (reading == null)
            {
                return NotFound();
            }

            return Ok(reading);
        }
        [HttpPost]
        [ResponseType(typeof(Reading))]
        public async Task<IHttpActionResult> PostReading(ReadingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var reading = new Reading
            {
                CH4 = model.CH4,
                CO = model.CO,
                CO2 = model.CO2,
                Created = DateTime.UtcNow,
                Dust = model.Dust,
                Hum = model.Hum,
                LPG = model.LPG,
                Preassure = model.Preassure,
                Temp = model.Temp
            };
            var sensor = await _db.Sensors.FirstOrDefaultAsync(f => f.TrackingKey == model.SensorTrackingKey);
            if (sensor == null)
            {
                return NotFound();
            }
            reading.SensorId = sensor.Id;
            _db.Readings.Add(reading);
            await _db.SaveChangesAsync();
            var hubReadings = new SensorReadingDispatchModel
            {
                SensorId = sensor.Id,
                Reading = new SensorReadingModel
                {
                    CH4 = reading.CH4,
                    CO = reading.CO,
                    CO2 = reading.CO2,
                    Dust = reading.Dust,
                    Hum = reading.Hum,
                    LPG = reading.LPG,
                    Preassure = reading.Preassure,
                    Temp = reading.Temp,
                    Created = reading.Created
                }
            };
            _hubContext.Clients.All.DispatchReading(hubReadings);
            return CreatedAtRoute("DefaultApi", new { id = reading.Id }, reading);
        }

        [ResponseType(typeof(Reading))]
        [HttpDelete]
        public IHttpActionResult DeleteReading(int id)
        {
            Reading reading = _db.Readings.Find(id);
            if (reading == null)
            {
                return NotFound();
            }
            _db.Readings.Remove(reading);
            _db.SaveChanges();

            return Ok(reading);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReadingExists(int id)
        {
            return _db.Readings.Count(e => e.Id == id) > 0;
        }
    }
}