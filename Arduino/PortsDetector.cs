﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Arduino
{
    public static class PortsDetector
    {

        public static (SerialPort arduino, SerialPort gps) DetectPorts(bool gpsRequired = true)
        {
            string[] ports = SerialPort.GetPortNames();
            var arduinoPort = DetectArduinoPort(ports);
            if(arduinoPort == null)
            {
                throw new KeyNotFoundException("Устройство для считывания показаний не подключено!"); 
            }
            ports = ports.Where(s => s != arduinoPort.PortName).ToArray();
            var gpsPort = DetectGPSPort(ports);
            if (gpsPort == null && gpsRequired)
            {
                throw new KeyNotFoundException("Устройство для считывания координат не подключено!");
            }
            return (arduinoPort, gpsPort);

        }

        private static SerialPort DetectArduinoPort(string[] portNames)
        {
            SerialPort arduino = null;
            foreach(var portName in portNames)
            {
                arduino = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                if (!arduino.IsOpen)
                {
                    arduino.Open();
                }
                arduino.WriteLine("1");
                Thread.Sleep(6000);
                var data = arduino.ReadExisting();
                arduino.Close();
                if (string.IsNullOrEmpty(data))
                {
                    arduino = null;
                }
                else
                {
                    break;
                }
            }
            return arduino;
        }

        private static SerialPort DetectGPSPort(string[] portNames)
        {
            SerialPort gps = null;
            foreach (var portName in portNames)
            {
                gps = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                if (!gps.IsOpen)
                {
                    gps.Open();
                }
                Thread.Sleep(1000);
                var dataFromDevice = gps.ReadExisting();
                gps.Close();
                if (string.IsNullOrEmpty(dataFromDevice) || !dataFromDevice.Contains("$GPRMC"))
                {
                    gps = null;
                }
                else
                {
                    break;
                }
            }
            return gps;
        }
    }
}
