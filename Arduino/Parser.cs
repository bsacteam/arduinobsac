﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arduino
{
    public static class Parser
    {
        public static ReadingModel ParseStringToData(string thread)
        {
            var table = GetTableDataAsListString(thread);
            var data = GetDataFromList(table);
            return data;
        }

        private static List<string> GetTableDataAsListString(string data)
        {
            return data.Split(' ').ToList();
        }

        private static ReadingModel GetDataFromList(List<string> items)
        {
            if (!(items.Any() || items.Count == 6))
            {
                return null;
            }
            var data = new ReadingModel
            {
                CO2 = float.Parse(items[0], CultureInfo.InvariantCulture.NumberFormat),
                LPG = float.Parse(items[1], CultureInfo.InvariantCulture.NumberFormat),
                CO = float.Parse(items[2], CultureInfo.InvariantCulture.NumberFormat),
                CH4 = float.Parse(items[3], CultureInfo.InvariantCulture.NumberFormat),
                Dust = float.Parse(items[4], CultureInfo.InvariantCulture.NumberFormat),
                Temp = float.Parse(items[5], CultureInfo.InvariantCulture.NumberFormat),
                Hum = float.Parse(items[6], CultureInfo.InvariantCulture.NumberFormat),
                Preassure = float.Parse(items[7], CultureInfo.InvariantCulture.NumberFormat)
            };
            return data;

        }

        public static SensorModel GetCoordinatesFromString(string data)
        {
            var model = new SensorModel();
            var gprms = data.Substring(data.LastIndexOf("$GPRMC"), 49);
            var latitudeStarts = gprms.LastIndexOf("V") + 2;
            var latitudeString = gprms.Substring(latitudeStarts, 9);
            var longitudeStarts = gprms.LastIndexOf("N") + 3;
            var longitudeString = gprms.Substring(longitudeStarts, 9);
            double.TryParse(latitudeString, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out var latitude);
            double.TryParse(longitudeString, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out var longitude);
            model.Latitude = latitude / 100;
            model.Longitude = longitude / 100;
            return model;
        }
    }
}
