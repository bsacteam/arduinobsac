﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arduino
{
    public interface IDataProcessor
    {
        void Process(ReadingModel data, string url);
    }
}
