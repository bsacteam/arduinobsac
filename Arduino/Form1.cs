﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Common.Models;
using Arduino.Properties;
using System.Configuration;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Timers;

namespace Arduino
{
    public partial class Form1 : Form
    {
        private List<ReadingModel> _readings = new List<ReadingModel>();
        private static readonly string _trackingCodeSettingName = "TrackingCode";
        private static readonly Configuration _config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
        private static readonly Random _random = new Random(DateTime.UtcNow.Minute);
        private static SerialPort _arduino;
        private static SerialPort _gps;
        private static System.Timers.Timer _timer;
        public Form1()
        {
            InitializeComponent();
            ServerUrl.Text = GetServerUrl();
        }

        private void InitializeTrackingKey()
        {
            if (!HasTrackingKeySettedUp())
            {
                RegisterSensorInSystem();
            }
            else
            {
                if (!CheckSensorTrackingKey())
                {
                    RegisterSensorInSystem();
                }
            }
        }

        private void RegisterSensorInSystem()
        {
            var client = new HttpClient();
            var sensorModel = GetCoordinates();
            var content = new StringContent(JsonConvert.SerializeObject(sensorModel), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{GetServerUrl()}/api/sensors", content).Result;
            _config.AppSettings.Settings.Remove(_trackingCodeSettingName);
            _config.AppSettings.Settings.Add(_trackingCodeSettingName, JsonConvert.DeserializeObject<string>(result.Content.ReadAsStringAsync().Result));
            _config.Save(ConfigurationSaveMode.Modified);
        }

        private bool CheckSensorTrackingKey()
        {
            var client = new HttpClient();
            var trackingKey = GetSensorTrackingKey();
            if (string.IsNullOrEmpty(trackingKey))
            {
                return false;
            }
            var uriBuilder = new UriBuilder($"{GetServerUrl()}/api/sensors/checkTrackingKey");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["key"] = trackingKey;
            uriBuilder.Query = parameters.ToString();
            var uri = uriBuilder.Uri.ToString();
            var response = client.GetAsync(uri).Result;
            var result = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
            Boolean.TryParse(result, out var boolResult);
            return boolResult;

        }

        private SensorModel GetCoordinates()
        {
            if (_gps == null)
            {
                throw new Exception("Cannot find GPS or device is not connected");
            }
            if (!_gps.IsOpen)
            {
                _gps.Open();
            }
            var dataFromDevice = _gps.ReadExisting();
            return Parser.GetCoordinatesFromString(dataFromDevice);
        }

        private string GetSensorTrackingKey()
        {
            return _config.AppSettings.Settings[_trackingCodeSettingName].Value;
        }

        private string GetServerUrl()
        {
            try
            {
                return _config.AppSettings.Settings["ServerUrl"].Value;
            }
            catch
            {
                return null;
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                InitializePorts();
                InitializeTrackingKey();
                Task.Run(() =>
                {
                    label1.PerformSafely(() => { label1.Visible = false; });
                    _timer = new System.Timers.Timer();
                    _timer.Interval = 6000;
                    _timer.Elapsed += timer1_Tick;
                    _timer.Enabled = true;
                    StartButton.PerformSafely(() => StartButton.Visible = false);
                });
            }
            catch(Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        private void ApplyServerUrlButton_Click(object sender, EventArgs e)
        {
            _config.AppSettings.Settings.Remove("ServerUrl");
            _config.AppSettings.Settings.Add("ServerUrl", ServerUrl.Text);
            _config.Save(ConfigurationSaveMode.Modified);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                var reading = GetReadingFromThread();
                reading.SensorTrackingKey = GetSensorTrackingKey();
                IDataProcessor processor = new DataServerProcessor();
                processor.Process(reading, $"{GetServerUrl()}/api/readings");
                _readings.Insert(0, reading);
                var source = new BindingSource(_readings, null);
                dataGridView1.PerformSafely(() => dataGridView1.DataSource = source);
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        private ReadingModel GetReadingFromThread()
        {
            if (!_arduino.IsOpen)
            {
                _arduino.Open();
            }
            _arduino.WriteLine("1");
            Thread.Sleep(4000);
            string potok = _arduino.ReadExisting();//записывает поток данных в строку
            if (string.IsNullOrEmpty(potok))
            {
                return null;
            }
            return Parser.ParseStringToData(potok);
        }

        private void ShowError(string message)
        {
            label1.PerformSafely(() =>
            {
                label1.Text = message;
                label1.Visible = true;
            });
        }

        private bool HasTrackingKeySettedUp()
        {
            return !_config.AppSettings.Settings.AllKeys.Any(f => f == _trackingCodeSettingName);
        }

        private void InitializePorts()
        {
            if (_arduino != null && _arduino.IsOpen)
            {
                _arduino.Close();
            }
            if (_gps != null && _gps.IsOpen)
            {
                _gps.Close();
            }
            var result = PortsDetector.DetectPorts(!HasTrackingKeySettedUp());
            _arduino = result.arduino;
            _gps = result.gps;
        }
    }
}
