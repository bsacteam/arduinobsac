﻿using System;
using Arduino;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArduinoTests
{
    [TestClass]
    public class ArduinoTests
    {

        [TestMethod]
        public void CheckPortsData()
        {
            var coordinates = Parser.GetCoordinatesFromString(@"$GPGGA,121722.820,5355.4559,N,02739.9643,E,0,0,,220.9,M,26.0,M,,*4D 
$GPGSA,A,1,,,,,,,,,,,,,,,*1E 
$GPGSV,3,1,10,25,84,264,,29,51,241,,02,51,079,,12,49,115,*7A 
$GPGSV,3,2,10,31,37,303,37,06,20,042,,14,15,280,29,05,05,117,*7E 
$GPGSV,3,3,10,24,05,172,,32,04,254,*79 
$GPRMC,121722.820,V,5355.4559,N,02739.9643,E,0.00,0.00,190518,,,N*71 
$GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32 
$GPGGA,121723.820,5355.4559,N,02739.9643,E,0,0,,220.9,M,26.0,M,,*4C 
$GPGSA,A,1,,,,,,,,,,,,,,,*1E 
$GPGSV,3,1,10,25,84,264,,29,51,241,,02,51,079,,12,49,115,*7A 
$GPGSV,3,2,10,31,37,303,37,06,20,042,,14,15,280,29,05,05,117,*7E 
$GPGSV,3,3,10,24,05,172,,32,04,254,*79 
$GPRMC,121723.820,V,5355.4559,N,02739.9643,E,0.00,0.00,190518,,,N*70 
$GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32");
            Assert.IsTrue(coordinates != null && coordinates.Latitude == 53.554559 && coordinates.Longitude == 27.399643);
        }
    }
}
